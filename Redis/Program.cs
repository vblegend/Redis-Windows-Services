﻿using log4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration.Install;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;

namespace Redis
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        static void Main(String[] Args)
        {
            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo(AppDomain.CurrentDomain.BaseDirectory + "log4net.config"));
            if (Args != null && Args.Length > 0)
            {
                foreach (var item in Args)
                {
                    switch (item.ToUpper())
                    {
                        case "-INSTALL":
                            d_logger.Debug("Install Windwos Services");
                            InstallServer(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName, true);
                            return;
                        case "-UNINSTALL":
                            d_logger.Debug("UnInstall Windwos Services");
                            InstallServer(System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName, false);
                            return;
                        default:
                            break;
                    }
                }
            }
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new Redis()
            };
            ServiceBase.Run(ServicesToRun);
        }






        /// <summary>
        /// 安装服务
        /// using System.Configuration.Install
        /// </summary>
        /// <param name="AssemblyFile">程序集</param>
        /// <param name="Install">true 安装  false 卸载</param>
        public static void InstallServer(String AssemblyFile, Boolean Install = true)
        {
            TransactedInstaller transactedInstaller = new TransactedInstaller();
            List<String> arrayList = new List<String>();
            Assembly assembly = null;
            //arrayList.Add("/LogToConsole=false");
            //XXServer, Version=3.425.2015.320, Culture=neutral, PublicKeyToken=null
            if (AssemblyFile.Contains("Version") && AssemblyFile.Contains("Culture") && AssemblyFile.Contains("PublicKeyToken"))
            {
                assembly = Assembly.Load(AssemblyFile);
            }
            else
            {
                assembly = Assembly.LoadFrom(AssemblyFile);
            }
            AssemblyInstaller value = new AssemblyInstaller(assembly, arrayList.ToArray());
            transactedInstaller.Installers.Add(value);
            transactedInstaller.Context = new InstallContext("Logs\\Install.txt", arrayList.ToArray());
            try
            {
                IDictionary stateSaver = new Hashtable();
                if (Install)
                {
                    transactedInstaller.Install(stateSaver);
                }
                else
                {
                    transactedInstaller.Uninstall(null);
                }

            }
            catch (Exception ex)
            {
                d_logger.Debug(ex.Message);
                d_logger.Debug(ex);
            }
        }


        private static readonly ILog d_logger = LogManager.GetLogger("Debug");

    }
}
