# Redis Windows Services

#### 项目介绍
 Windows服务方式承载 Redis
 实现以服务方式启动，可以在用户登录之前启动。
 Redis 进程关闭自动重启，
 暂时未实现进程无响应或异常自动重启功能

 
 

#### 软件架构
Redis 版本  2016-12-01
理论支持任意版本 下载到同目录下


#### 安装教程

1. 安装服务 Redis.exe -Install
2. 启动服务 Net Start Redis
3. 停止服务 Net Stop Redis
4. 卸载服务 Redis.exe -UnInstall
#### 使用说明

1. 安装后服务默认为停止状态，需要手动启动服务
2. 默认开启安全验证 密码123456 
   修改密码 redis.windows.conf -> masterauth
3. 修改配置 修改目录下redis.windows.conf 配置文件